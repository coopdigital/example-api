#!/bin/bash
# This script should be invoked as an operational script to run every certain time period. Or in the event
# of a physical infrastructure failure or Kubernetes failure where runtimes and server groups have been left in
# an orphaned state (where containers have been destroyed but not removed from ARM)
# *ANYPOINT_USERNAME: The Anypoint username to log into the platform with
# *ANYPOINT_PASSWORD: The Anypoint password to log into the platform with
# *ANYPOINT_ORG: The name of the top level business group within Anypoint
# *ANYPOINT_ENV: The environment in Anypoint to reconcile servers against
# *ANYPOINT_HOST: If using PCE the host name of the Platform- do not include the protocol
export ANYPOINT_USERNAME
export ANYPOINT_PASSWORD
export ANYPOINT_ORG
export ANYPOINT_ENV
export SERVER_GROUP


function checkServerGroupStatus {
  echo "Checking server group status for groupID: ${1}"

  SERVER_GROUP_STATUS=$(anypoint-cli runtime-mgr serverGroup describe $1)
  if [[ $SERVER_GROUP_STATUS == *"EMPTY"* ]]; then
    echo "Server Group is empty deleting the group"
    anypoint-cli runtime-mgr serverGroup delete $1
  fi
}

function checkServerStatus {
  echo "Checking server status for serverID: $1"
  SERVER_STATUS=$(anypoint-cli runtime-mgr server describe $1 -f Status -o text)
  #Check if the Server is disconnected
  if [[ $SERVER_STATUS == *"DISCONNECTED"* ]]
    then
      echo "Server ID: $1 is disconnected. Removing it."
      DELETION_RESPONSE=$(anypoint-cli runtime-mgr server delete $1)
      #Check If the server belongs to a server group- need to remove it from the server group first
      if [[ $DELETION_RESPONSE == *"belongs to a Server Group"* ]]
      then
        echo "Server ID: $1 belongs to a server group removing it from the server group"
        SEVER_GRP_ID=$(anypoint-cli runtime-mgr server describe $1 -f "Server Group Id" -o text | tr -dc '[:print:]' | sed 's/\[90m\[39m/ /g'| awk '{print $4}' )
        anypoint-cli runtime-mgr serverGroup remove server ${SEVER_GRP_ID} $1
		anypoint-cli runtime-mgr server delete $1
      fi
  #Check if the Server is in Created state which means it has never started
  else if [[ $SERVER_STATUS == *"CREATED"* ]]
    then
      echo "Server ID: $1 never started. Removing it."
      anypoint-cli runtime-mgr server delete $1
    fi
  fi
}

function checkForEmptyServerGroups {
  echo "Checking if any empty server groups exist for API: $SERVER_GROUP"
  ALL_SERVER_GROUPS_FOR_API=($(anypoint-cli runtime-mgr serverGroup list -f ID,Name -o text | grep $SERVER_GROUP | awk '{print $1}' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" | sed 's/ID//g'| sed 's/Error: Environment not found//g' | xargs ))
  for i in "${ALL_SERVER_GROUPS_FOR_API[@]}"
  do
    #Check to see if server needs to be removed
    checkServerGroupStatus ${i}
  done

}

function getServersForAPI {
  echo "Getting all servers for API: $SERVER_GROUP"
  
  ALL_SERVERS_FOR_API=($(anypoint-cli runtime-mgr server list -f ID,Name -o text | grep  $SERVER_GROUP | awk '{print $1}' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" | sed 's/ID//g'| sed 's/Error: Environment not found//g' | xargs ))
  ## Loop through all the servers
  for i in "${ALL_SERVERS_FOR_API[@]}"
  do
     #Check to see if server needs to be removed
    checkServerStatus ${i}
  done
  #Check to see if any empty server groups exist
  checkForEmptyServerGroups

}

echo "Welcome to Anypoint clean up"
echo "Checking servers for the org: $ANYPOINT_ORG and Env: $ANYPOINT_ENV and API: $SERVER_GROUP"
getServersForAPI 
echo "Done with clean up"
